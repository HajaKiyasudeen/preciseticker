//
//  UserModel.swift
//  Believer
//
//  Created by Apple on 15/03/2021.
//

import Foundation
import RealmSwift

class User: Object {
    @objc dynamic var name = ""
    @objc dynamic var mobile : String = ""
    @objc dynamic var password : String = ""
}
