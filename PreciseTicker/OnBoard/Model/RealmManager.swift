//
//  RealmManager.swift
//  PreciseTicker
//
//  Created by Apple on 15/03/2021.
//

import Foundation
import RealmSwift

class RealmManager {
    
    public static func getRealmInstance() -> Realm {
        let realm = try! Realm();
        return realm;
    }
    
    public static var sharedObj: RealmManager?;

    
    public static func shared() -> RealmManager {
        if (sharedObj == nil) {
            initialize();
        }
        return sharedObj!;
    }

    public static func initialize()
    {
        RealmManager.sharedObj = RealmManager();
    }

    private init() {
    }

    public func saveUserData(value : [String : Any]){

    }
}
