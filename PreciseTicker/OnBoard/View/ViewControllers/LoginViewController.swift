//
//  LoginViewController.swift
//  PreciseTicker
//
//  Created by Apple on 15/03/2021.
//

import UIKit
import Starscream

class LoginViewController: UIViewController {

    @IBOutlet fileprivate var userNameTextField : UITextField!
    @IBOutlet fileprivate var passwordTextField : UITextField!

    @IBOutlet fileprivate var loginButton : RoundedCornerButton!

    @IBOutlet fileprivate var forgotPasswordButton : UIButton!

    @IBOutlet fileprivate var registerButton : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setUpViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.isHidden = true
    }
    
    //MARK:- setUpViews
    func setUpViews() {
        view.backgroundColor = UIColor.white

        //userNameTextField
        userNameTextField.textColor = UIColor.black
        userNameTextField.attributedPlaceholder = NSAttributedString(string: LConstant.enterUserName, attributes: [
                                                                            NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        
        //passwordTextField
        passwordTextField.textColor = UIColor.black
        passwordTextField.isSecureTextEntry = true
        passwordTextField.attributedPlaceholder = NSAttributedString(string: LConstant.enterPassword, attributes: [
                                                                            NSAttributedString.Key.foregroundColor : UIColor.lightGray])

        
        //loginButton
        loginButton.setTitle(LConstant.login, for: .normal)
        loginButton.addTarget(self, action: #selector(loginButtonAction), for: .touchUpInside)

        
        //forgotPasswordButton
        forgotPasswordButton.setTitle(LConstant.forgotPassword, for: .normal)
        forgotPasswordButton.addTarget(self, action: #selector(forgotPasswordButtonAction), for: .touchUpInside)

        
        //registerButton
        registerButton.setTitleColor(UIColor.white, for: .normal)
        registerButton.setTitle(LConstant.registerDesc, for: .normal)
        registerButton.addTarget(self, action: #selector(registerButtonAction), for: .touchUpInside)
    }
    
    //MARK:- Actions
    @objc func loginButtonAction() {
        
//        guard validateFields() else {
//            return
//        }

        let dict : [String: Any] = ["command" : "subscribe",
                                    "channel" :1002]
        if let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
        {
            SocketManager.shared.writeData(data: data)
        }
    }
    
    func validateFields() -> Bool {

        let userName = userNameTextField.text!
        let password = passwordTextField.text!

        if userName != "" && password != "" {
            
            if password.count > 6 {
                return true
            }
            return false
        }
        else {
            return false
        }

    }
    
    
    @objc func forgotPasswordButtonAction() {
     
        showAlert(message: "Inprogress")
    }
    
    @objc func registerButtonAction() {
        
        if let registerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegisterViewController") as? RegisterViewController {
            navigationController?.pushViewController(registerVC, animated: true)
        }
    }
}
