//
//  RegisterViewController.swift
//  PreciseTicker
//
//  Created by Apple on 15/03/2021.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet fileprivate var backButton : UIButton!

    @IBOutlet fileprivate var userNameTextField : UITextField!
    @IBOutlet fileprivate var mobileNumberTextField : UITextField!
    @IBOutlet fileprivate var passwordTextField : UITextField!
    @IBOutlet fileprivate var confirmPasswordTextField : UITextField!

    @IBOutlet fileprivate var registerButton : RoundedCornerButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setUpViews()
    }
    
    //MARK:- Set Up views
    func setUpViews() {
        view.backgroundColor = UIColor.white
        
        //backButton
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        
        //userNameTextField
        userNameTextField.textColor = UIColor.black
        userNameTextField.attributedPlaceholder = NSAttributedString(string: LConstant.enterUserName, attributes: [
                                                                            NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        
        //mobileNumberTextField
        mobileNumberTextField.textColor = UIColor.black
        mobileNumberTextField.attributedPlaceholder = NSAttributedString(string: LConstant.enterMobileNumber, attributes: [
                                                                            NSAttributedString.Key.foregroundColor : UIColor.lightGray])

        //passwordTextField
        passwordTextField.textColor = UIColor.black
        passwordTextField.isSecureTextEntry = true
        passwordTextField.attributedPlaceholder = NSAttributedString(string: LConstant.enterPassword, attributes: [
                                                                            NSAttributedString.Key.foregroundColor : UIColor.lightGray])

        //confirmPasswordTextField
        confirmPasswordTextField.textColor = UIColor.black
        confirmPasswordTextField.isSecureTextEntry = true
        confirmPasswordTextField.attributedPlaceholder = NSAttributedString(string: LConstant.reEnterPassword, attributes: [
                                                                            NSAttributedString.Key.foregroundColor : UIColor.lightGray])

        //registerButton
        registerButton.setTitle(LConstant.register, for: .normal)
        registerButton.addTarget(self, action: #selector(registerButtonAction), for: .touchUpInside)

    }

    //MARK:- Actions
    @objc func backButtonAction() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func registerButtonAction() {
        
        let userName = userNameTextField.text!
        let mobileNumber = mobileNumberTextField.text!
        let password = passwordTextField.text!
        let confirmPassword = confirmPasswordTextField.text!

        if userName != "" && mobileNumber != "" && password != "" && confirmPassword != "" {

            if mobileNumber.count > 8 && mobileNumber.count < 11 {
                if password == confirmPassword {
                    
                    //Saving User Info
                    let userObj = User()
                    userObj.name = userName
                    userObj.mobile = mobileNumber
                    userObj.password = password
                
                    let realm = RealmManager.getRealmInstance()
                    try! realm.write {
                        realm.add(userObj)
                    }
                    
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.showAlert(message: "Password should be same")
                }
            }
            else {
                self.showAlert(message: "Enter Valid Mobile Number")
            }
        }
        else {
            showAlert(message: "Enter mandatory Fields")
        }
    }

}
