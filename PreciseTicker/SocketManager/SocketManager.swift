//
//  SocketManager.swift
//  PreciseTicker
//
//  Created by Apple on 15/03/2021.
//

import Foundation
import StompClientLib
import Starscream

class SocketManager {
    
    static let shared = SocketManager()

    var socket: WebSocket!
    var isConnected = false

    private var socketURL: URL {
        URL(string: "wss://api2.poloniex.com")!
    }
    
    init() {
        var request = URLRequest(url: socketURL)
        request.timeoutInterval = 5
        socket = WebSocket(request: request)
        socket.delegate = self
    }
}


// MARK: - Public Methods

extension SocketManager {
    
    func connectSocket() {
        socket.connect()
    }

    func disConnectSocket() {
        socket.disconnect()
    }
    
    func writeData(data: Data) {
        socket.write(data: data)
    }
}



//MARK: - StompClientLibDelegate

extension SocketManager: WebSocketDelegate {
    
    // MARK: - WebSocketDelegate
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
        case .connected(let headers):
            isConnected = true
            print("websocket is connected: \(headers)")
        case .disconnected(let reason, let code):
            isConnected = false
            print("websocket is disconnected: \(reason) with code: \(code)")
        case .text(let string):
            print("Received text: \(string)")
        case .binary(let data):
            print("Received data: \(data.count)")
        case .ping(_):
            break
        case .pong(_):
            break
        case .viabilityChanged(_):
            break
        case .reconnectSuggested(_):
            break
        case .cancelled:
            isConnected = false
        case .error(let error):
            isConnected = false
            handleError(error)
        }
    }
    
    func handleError(_ error: Error?) {
        if let e = error as? WSError {
            print("websocket encountered an error: \(e.message)")
        } else if let e = error {
            print("websocket encountered an error: \(e.localizedDescription)")
        } else {
            print("websocket encountered an error")
        }
    }
}
