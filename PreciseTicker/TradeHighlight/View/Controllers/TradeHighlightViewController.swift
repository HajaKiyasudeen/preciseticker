//
//  TradeHighlightViewController.swift
//  PreciseTicker
//
//  Created by Apple on 16/03/2021.
//

import UIKit

class TradeHighlightViewController: UIViewController {

    @IBOutlet fileprivate var valueTextField : UITextField!

    @IBOutlet fileprivate var tableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setUpView()
    }
    

    func setUpView(){
        
        //valueTextField
        valueTextField.delegate = self
        valueTextField.textColor = UIColor.black
        valueTextField.attributedPlaceholder = NSAttributedString(string: "Enter Value", attributes: [
                                                                            NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        valueTextField.addTarget(self, action: #selector(textFieldValueChanged), for: .editingChanged)
    }

    @objc func textFieldValueChanged(textField: UITextField) {
        
        if textField.text!.count > 0 {
            
        }
    }
}

extension TradeHighlightViewController: UITextFieldDelegate {
    
    
}
