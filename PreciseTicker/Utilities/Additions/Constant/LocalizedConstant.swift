//
//  LocalizedConstant.swift
//  PreciseTicker
//
//  Created by Apple on 15/03/2021.
//

import Foundation

enum LConstant {
    
    //Common
    static var login: String { NSLocalizedString("Common.Login", comment: "") }
    static var register: String { NSLocalizedString("Common.Register", comment: "") }

    static var registerDesc: String { NSLocalizedString("Login.RegisterDesc", comment: "") }

    static var enterUserName: String { NSLocalizedString("Common.EnterUserName", comment: "") }
    
    static var enterMobileNumber: String { NSLocalizedString("Common.EnterMobileNum", comment: "") }

    static var enterPassword: String { NSLocalizedString("Common.EnterPassword", comment: "") }
    static var reEnterPassword: String { NSLocalizedString("Common.ReEnterPassword", comment: "") }

    static var forgotPassword: String { NSLocalizedString("Login.ForgotPassword", comment: "") }
}
