//
//  RoundedCornerButton.swift
//  PreciseTicker
//
//  Created by Apple on 15/03/2021.
//

import UIKit

@IBDesignable
class RoundedCornerButton: UIButton {
    
    @IBInspectable
    var bgColor : UIColor = UIColor.red {
        didSet {
            self.backgroundColor = bgColor
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    func commonInit() {
        self.clipsToBounds = true
        self.setTitleColor(UIColor.white, for: .normal)
        self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        self.setTitleColor(UIColor.white, for: .normal)
    }

    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = cornerRadius
    }
}
